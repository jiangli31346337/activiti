package cc.sunni.activiti;

import java.io.Serializable;

/**
 * @author jl
 * @since 2021/1/25 22:42
 */
public class UELPOJO implements Serializable {

    private String assignee;

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }
}
