package cc.sunni.activiti;

import org.activiti.api.runtime.shared.query.Page;
import org.activiti.api.runtime.shared.query.Pageable;
import org.activiti.api.task.model.Task;
import org.activiti.api.task.model.builders.TaskPayloadBuilder;
import org.activiti.api.task.runtime.TaskRuntime;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author jl
 * @since 2021/1/26 23:04
 */
@SpringBootTest
public class TaskRuntimeTest {
    @Autowired
    private TaskRuntime taskRuntime;
    @Autowired
    private SecurityUtil securityUtil;

    /**
     * 获取当前登录用户任务
     */
    @Test
    public void getTask() {
        securityUtil.logInAs("bajie");
        Page<Task> tasks = taskRuntime.tasks(Pageable.of(0, 100));
        for (Task task : tasks.getContent()) {
            System.out.println("id:" + task.getId());
            System.out.println("name:" + task.getName());
            System.out.println("status:" + task.getStatus());
            System.out.println("businessKey:" + task.getBusinessKey());
            System.out.println("processInstanceId:" + task.getProcessInstanceId());
            if (task.getAssignee() == null) {
                // 候选人为当前用户,需要拾取任务
                System.out.println("当前用户为候选人,可以拾取任务");
            } else {
                System.out.println("assignee :" + task.getAssignee());
            }
            System.out.println("--------------");
        }
    }

    /**
     * 完成任务
     */
    @Test
    public void completeTask() {
        securityUtil.logInAs("bajie");
        Task task = taskRuntime.task("b5818565-5f0d-11eb-98c0-005056c00001");
        if (task.getAssignee() == null) {
            // 拾取任务
            taskRuntime.claim(TaskPayloadBuilder.claim().withTaskId(task.getId()).build());
        }
        // 完成任务
        Task complete = taskRuntime.complete(TaskPayloadBuilder.complete().withTaskId(task.getId()).build());
        System.out.println("任务完成,taskId:" + complete.getId());
    }
}
