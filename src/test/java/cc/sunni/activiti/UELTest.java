package cc.sunni.activiti;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author jl
 * @since 2021/1/25 21:52
 */
@SpringBootTest
public class UELTest {
    @Autowired
    private RepositoryService repositoryService;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private TaskService taskService;

    /**
     * 第一步:部署流程
     */
    @Test
    public void initDeploymentBPMN() {
        String fileName = "bpmn/UEL_V3.bpmn";
        Deployment deployment = repositoryService.createDeployment()
                .addClasspathResource(fileName)
                .name("UEL_V3流程")
                .deploy();
        System.out.println(deployment.getId());
    }

    /**
     * 创建流程实例带参数
     */
    @Test
    public void initProcessInstanceWithArgs() {
        // ${assignee} 动态指定第一个任务节点的执行人
        Map<String, Object> map = new HashMap<>();
        map.put("assignee", "wukong");
        String businessKey = UUID.randomUUID().toString();
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("myProcess_UEL_V1", businessKey, map);
        System.out.println("流程实例ID:" + processInstance.getProcessInstanceId());
    }

    /**
     * 执行任务带参数
     */
    @Test
    public void completeTaskWithArgs() {
        // 排他网关的条件
        Map<String, Object> map = new HashMap<>();
        map.put("pay", 101);
        taskService.complete("5789e266-5f18-11eb-8637-005056c00001", map);
    }

    /**
     * 创建流程实例使用实体类作为参数
     */
    @Test
    public void initProcessInstanceWithPojo() {
        UELPOJO uelpojo = new UELPOJO();
        uelpojo.setAssignee("bajie");
        Map<String, Object> map = new HashMap<>();
        map.put("uelpojo", uelpojo);
        String businessKey = UUID.randomUUID().toString();
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("myProcess_UEL_V3", businessKey, map);
        System.out.println("流程实例ID:" + processInstance.getProcessInstanceId());
    }

    /**
     * 执行任务带参数,指定多个候选人
     */
    @Test
    public void completeTaskWithCandidate() {
        // 指定多个候选人
        Map<String, Object> map = new HashMap<>();
        map.put("candidate", "wukong,tangsen");
        taskService.complete("d32a6a16-5f1b-11eb-b0a6-005056c00001", map);
    }

    /**
     * 设置全局流程变量
     */
    @Test
    public void otherArgs() {
        runtimeService.setVariable("d32a6a16-5f1b-11eb-b0a6-005056c00001","candidate","wukong,tangsen");
//        runtimeService.setVariables();
//        taskService.setVariable();
//        taskService.setVariables();
    }

    /**
     * 设置局部流程变量
     */
    @Test
    public void otherLocalArgs() {
        runtimeService.setVariableLocal("d32a6a16-5f1b-11eb-b0a6-005056c00001","candidate","wukong,tangsen");
//        runtimeService.setVariablesLocal();
//        taskService.setVariableLocal();
//        taskService.setVariablesLocal();
    }

}
