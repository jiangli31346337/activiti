package cc.sunni.activiti;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.ProcessDefinition;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author jl
 * @since 2021/1/25 18:24
 * 流程定义
 */
@SpringBootTest
public class ProcessDefinitionTest {
    @Autowired
    private RepositoryService repositoryService;

    @Test
    public void getProcessDefinition() {
        List<ProcessDefinition> list = repositoryService.createProcessDefinitionQuery().list();
        for (ProcessDefinition processDefinition : list) {
            System.out.println(processDefinition.getId()); // key:version:uuid
            System.out.println(processDefinition.getKey());
            System.out.println(processDefinition.getVersion());
            System.out.println(processDefinition.getName());
            System.out.println(processDefinition.getResourceName());
            System.out.println(processDefinition.getDeploymentId());
            System.out.println("--------------------");
        }
    }

    /**
     * 删除流程定义和部署
     */
    @Test
    public void delProcessDefinition() {
        String pdId = "2d0a364c-5ed9-11eb-91df-005056c00001";
        repositoryService.deleteDeployment(pdId,true);
    }
}

