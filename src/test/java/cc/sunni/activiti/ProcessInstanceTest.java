package cc.sunni.activiti;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.UUID;

/**
 * @author jl
 * @since 2021/1/25 18:48
 * 流程实例
 */
@SpringBootTest
public class ProcessInstanceTest {
    @Autowired
    private RuntimeService runtimeService;

    /**
     * 新增.初始化一个流程实例
     */
    @Test
    public void initProcessInstance() {
        // 1.获取页面表单填报的内容. 请假时间,请假理由,申请人等
        // 2.写入业务表,返回业务表的主键ID=businessKey
        // 3.把业务数据与Activiti流程数据关联
        String businessKey = UUID.randomUUID().toString();
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("myProcess_2", "1");
        System.out.println("流程实例ID:"+processInstance.getProcessInstanceId());
    }

    /**
     * 查询流程实例列表
     */
    @Test
    public void getProcessInstances() {
        List<ProcessInstance> list = runtimeService.createProcessInstanceQuery().list();
        for (ProcessInstance processInstance : list) {
            System.out.println(processInstance.getProcessInstanceId());
            System.out.println(processInstance.getProcessDefinitionId());
            System.out.println(processInstance.isEnded());
            System.out.println(processInstance.isSuspended()); // 暂停
            System.out.println("---------------");
        }
    }

    /**
     * 挂起与激活流程实例
     */
    @Test
    public void activeProcessInstance() {
        // 暂停
//        runtimeService.suspendProcessInstanceById("3bfd6b3a-5efe-11eb-b02a-005056c00001");

        // 激活
        runtimeService.activateProcessInstanceById("3bfd6b3a-5efe-11eb-b02a-005056c00001");
    }

    /**
     * 删除流程实例
     */
    @Test
    public void delProcessInstance() {
        runtimeService.deleteProcessInstance("c7b41033-5eff-11eb-bd45-005056c00001","测试");
    }
}
