package cc.sunni.activiti;

import org.activiti.engine.HistoryService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author jl
 * @since 2021/1/25 21:23
 */
@SpringBootTest
public class HistoryTaskTest {
    @Autowired
    private HistoryService historyService;

    /**
     * 查询我的经办
     */
    @Test
    public void getHistoryTaskByUserId() {
        List<HistoricTaskInstance> list = historyService.createHistoricTaskInstanceQuery()
                .orderByHistoricTaskInstanceEndTime().asc()
                .taskAssignee("bajie")
                .list();
        for (HistoricTaskInstance historicTaskInstance : list) {
            System.out.println("Id:"+historicTaskInstance.getId());
            System.out.println("ProcessDefinitionId:"+historicTaskInstance.getProcessDefinitionId());
            System.out.println("Name:"+historicTaskInstance.getName());
            System.out.println("Assignee:"+historicTaskInstance.getAssignee());
            System.out.println("EndTime:"+historicTaskInstance.getEndTime());
            System.out.println("------------------");
        }
    }

    /**
     * 根据流程实例ID查询历史
     */
    @Test
    public void getHistoryTaskByProcessInstanceId() {
        List<HistoricTaskInstance> list = historyService.createHistoricTaskInstanceQuery()
                .orderByHistoricTaskInstanceEndTime().asc()
                .processInstanceId("9d3e72b2-5f06-11eb-a790-005056c00001")
                .list();
        for (HistoricTaskInstance historicTaskInstance : list) {
            System.out.println("Id:"+historicTaskInstance.getId());
            System.out.println("ProcessDefinitionId:"+historicTaskInstance.getProcessDefinitionId());
            System.out.println("Name:"+historicTaskInstance.getName());
            System.out.println("Assignee:"+historicTaskInstance.getAssignee());
            System.out.println("EndTime:"+historicTaskInstance.getEndTime());
            System.out.println("------------------");
        }
    }
}
