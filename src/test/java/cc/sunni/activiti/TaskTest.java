package cc.sunni.activiti;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.UUID;

/**
 * @author jl
 * @since 2021/1/25 20:08
 */
@SpringBootTest
public class TaskTest {
    @Autowired
    private RepositoryService repositoryService;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private TaskService taskService;


    /**
     * 第一步:部署流程
     */
    @Test
    public void initDeploymentBPMN() {
        String fileName = "bpmn/task_claim.bpmn";
        Deployment deployment =repositoryService.createDeployment()
                .addClasspathResource(fileName)
                .name("候选人流程任务")
                .deploy();
        System.out.println(deployment.getId());
    }

    /**
     * 第二步:创建流程实例
     */
    @Test
    public void initProcessInstance() {
        // 1.获取页面表单填报的内容. 报销时间,报销理由,报销申请人等
        // 2.写入业务表,返回业务表的主键ID=businessKey
        // 3.把业务数据与Activiti流程数据关联
        String businessKey = UUID.randomUUID().toString();
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("myProcess_UEL_V2", businessKey);
        System.out.println("流程实例ID:"+processInstance.getProcessInstanceId());
    }

    /**
     * 管理员查询所有任务
     */
    @Test
    public void getTasks() {
        List<Task> list = taskService.createTaskQuery().list();
        for (Task task : list) {
            System.out.println("Id:"+task.getId());
            System.out.println("Name:"+task.getName());
            System.out.println("Assignee:"+task.getAssignee());
            System.out.println("---------------");
        }
    }

    /**
     * 查询我的待办
     */
    @Test
    public void getTaskByAssignee() {
        List<Task> list = taskService.createTaskQuery().taskAssignee("wukong").list();
        for (Task task : list) {
            System.out.println("Id:"+task.getId());
            System.out.println("Name:"+task.getName());
            System.out.println("Assignee:"+task.getAssignee());
            System.out.println("---------------");
        }
    }

    /**
     * 执行任务
     */
    @Test
    public void completeTask() {
        taskService.complete("858fe0e2-5f0b-11eb-8231-005056c00001");
    }

    /**
     * 拾取任务
     */
    @Test
    public void claimTask() {
        // TODO: 2021/1/25  查询候选人的待办
//        taskService.createTaskQuery().taskCandidateUser("bajie").list();
        Task task = taskService.createTaskQuery().taskId("7b832fcb-5f1c-11eb-9f88-005056c00001").singleResult();
        taskService.claim("7b832fcb-5f1c-11eb-9f88-005056c00001","wukong");
    }

    /**
     * 归还、交办任务
     */
    @Test
    public void setTaskAssignee() {
        Task task = taskService.createTaskQuery().taskId("b5818565-5f0d-11eb-98c0-005056c00001").singleResult();
        taskService.setAssignee("b5818565-5f0d-11eb-98c0-005056c00001",null);  // 归还任务
       // taskService.setAssignee("b5818565-5f0d-11eb-98c0-005056c00001","wukong");  // 交办任务
    }


}
