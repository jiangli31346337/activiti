package cc.sunni.activiti;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

/**
 * @author jl
 * @since 2021/1/26 21:10
 */
@SpringBootTest
public class ParallelGatewayTest {
    @Autowired
    private RepositoryService repositoryService;
    @Autowired
    private RuntimeService runtimeService;
    @Autowired
    private TaskService taskService;

    /**
     * 第一步:部署流程
     */
    @Test
    public void initDeploymentBPMN() {
        String fileName = "bpmn/parallel_gateway.bpmn";
        Deployment deployment =repositoryService.createDeployment()
                .addClasspathResource(fileName)
                .name("并行网关流程")
                .deploy();
        System.out.println(deployment.getId());
    }

    /**
     * 第二步:创建流程实例
     */
    @Test
    public void initProcessInstance() {
        String businessKey = UUID.randomUUID().toString();
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("myProcess_parallel", businessKey);
        System.out.println("流程实例ID:"+processInstance.getProcessInstanceId());
    }

    /**
     * 执行任务
     */
    @Test
    public void completeTask() {
        taskService.complete("1119303a-5fd8-11eb-aafc-005056c00001");
    }

}
