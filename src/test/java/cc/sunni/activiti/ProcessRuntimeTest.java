package cc.sunni.activiti;

import org.activiti.api.model.shared.model.VariableInstance;
import org.activiti.api.process.model.ProcessInstance;
import org.activiti.api.process.model.builders.ProcessPayloadBuilder;
import org.activiti.api.process.runtime.ProcessRuntime;
import org.activiti.api.runtime.shared.query.Page;
import org.activiti.api.runtime.shared.query.Pageable;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.UUID;

/**
 * @author jl
 * @since 2021/1/26 22:17
 */
@SpringBootTest
public class ProcessRuntimeTest {
    @Autowired
    private ProcessRuntime processRuntime;
    @Autowired
    private SecurityUtil securityUtil;

    /**
     * 查询流程
     */
    @Test
    public void getProcessInstance() {
        securityUtil.logInAs("bajie");
        Page<ProcessInstance> processInstancePage = processRuntime.processInstances(Pageable.of(0, 100));
        int totalItems = processInstancePage.getTotalItems();
        List<ProcessInstance> list = processInstancePage.getContent();
        for (ProcessInstance processInstance : list) {
            System.out.println("id:" + processInstance.getId());
            System.out.println("name:" + processInstance.getName());
            System.out.println("startDate:" + processInstance.getStartDate());
            System.out.println("status:" + processInstance.getStatus());
            System.out.println("processDefinitionId:" + processInstance.getProcessDefinitionId());
            System.out.println("processDefinitionKey:" + processInstance.getProcessDefinitionKey());
            System.out.println("-------------------");
        }
    }

    /**
     * 启动流程
     */
    @Test
    public void startProcessInstance() {
        // 1.获取页面表单填报的内容. 报销时间,报销理由,报销申请人等
        // 2.写入业务表,返回业务表的主键ID=businessKey
        // 3.把业务数据与Activiti流程数据关联
        securityUtil.logInAs("bajie");
        ProcessInstance processInstance = processRuntime.start(ProcessPayloadBuilder.start()
                .withProcessDefinitionKey("myProcess_task")
                .withName("ProcessRuntime流程")
                .withBusinessKey(UUID.randomUUID().toString())
                .build());
    }

    /**
     * 删除流程
     */
    @Test
    public void deleteProcessInstance() {
        securityUtil.logInAs("bajie");
        ProcessInstance processInstance = processRuntime.delete(ProcessPayloadBuilder.delete()
                .withProcessInstanceId("d32009d0-5f1b-11eb-b0a6-005056c00001")
                .build());
    }

    /**
     * 挂起流程
     */
    @Test
    public void suspendProcessInstance() {
        securityUtil.logInAs("bajie");
        ProcessInstance processInstance = processRuntime.suspend(ProcessPayloadBuilder.suspend()
                .withProcessInstanceId("d32009d0-5f1b-11eb-b0a6-005056c00001")
                .build());
    }

    /**
     * 激活流程
     */
    @Test
    public void resumeProcessInstance() {
        securityUtil.logInAs("bajie");
        ProcessInstance processInstance = processRuntime.resume(ProcessPayloadBuilder.resume()
                .withProcessInstanceId("d32009d0-5f1b-11eb-b0a6-005056c00001")
                .build());
    }

    /**
     * 查询流程参数
     */
    @Test
    public void getVariable() {
        securityUtil.logInAs("bajie");
        List<VariableInstance> variables = processRuntime.variables(ProcessPayloadBuilder.variables()
                .withProcessInstanceId("b57ddbe1-5f0d-11eb-98c0-005056c00001")
                .build());
        for (VariableInstance variable : variables) {
            System.out.println("taskId:" + variable.getTaskId());
            System.out.println("name:" + variable.getName());
            System.out.println("value:" + variable.getValue());
            System.out.println("processInstanceId:" + variable.getProcessInstanceId());
            System.out.println("-------------------");
        }
    }


}
