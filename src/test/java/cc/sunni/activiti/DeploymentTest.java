package cc.sunni.activiti;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author jl
 * @since 2021/1/25 14:23
 * 流程部署
 */
@SpringBootTest
public class DeploymentTest {
    @Autowired
    private RepositoryService repositoryService;

    /**
     * 新增.通过bpmn部署流程
     */
    @Test
    public void initDeploymentBPMN() {
        String fileName = "bpmn/deployment.bpmn";
        Deployment deployment =repositoryService.createDeployment()
                .addClasspathResource(fileName)
                .name("流程部署测试2")
                .deploy();
        System.out.println(deployment.getId());
    }

    /**
     * 查询
     */
    @Test
    public void getDeployments() {
        List<Deployment> list = repositoryService.createDeploymentQuery().list();
        for (Deployment deployment : list) {
            System.out.println(deployment.getId());
            System.out.println(deployment.getKey());
            System.out.println(deployment.getDeploymentTime());
        }
    }
}
