package cc.sunni.activiti.config;

import lombok.Data;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author jiangli
 * @since 2021/1/6 16:37
 * 常量类，读取配置文件application.yml中的配置
 */
@Component
@ConfigurationProperties(prefix = "act")
@Data
public class PropertiesConfig implements InitializingBean {

    private String path;


    public static String PATH;

    @Override
    public void afterPropertiesSet() {
        PATH = path;
    }

}
