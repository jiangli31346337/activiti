package cc.sunni.activiti.security;

import cc.sunni.activiti.util.R;
import cn.hutool.json.JSONUtil;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * AuthenticationEntryPoint 用来解决匿名用户访问无权限资源时的异常
 */
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
//        response.setContentType("application/json; charset=utf-8");
//        response.setStatus(HttpStatus.UNAUTHORIZED.value());
//        PrintWriter writer = response.getWriter();
//        writer.print(JSONUtil.toJsonStr(R.error("尚未登录,请登录")));
//        writer.close();
//        response.flushBuffer();

        // 前后端不分离就跳转到首页
        response.sendRedirect("/layuimini/page/login-1.html");
    }

}