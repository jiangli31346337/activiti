package cc.sunni.activiti.security;

import cc.sunni.activiti.mapper.UserDao;
import cc.sunni.activiti.pojo.UserInfoBean;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * 我们自己定义的 UserService 需要实现 UserDetailsService 接口
 */
@Service
public class UserService implements UserDetailsService {
    @Autowired
    UserDao userDao;

    /**
     * 实现loadUserByUsername ，这个方法的参数就是用户在登录的时候传入的用户名，根据用户名去查询用户信息
     * （查出来之后，系统会自动进行密码比对）
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserInfoBean user = userDao.selectOne(new LambdaQueryWrapper<UserInfoBean>().eq(UserInfoBean::getUsername,username));
        if (user == null) {
            throw new UsernameNotFoundException("用户名或密码错误!");
        }
        if (!user.isEnabled()) {
            throw new DisabledException("该账户已被禁用，请联系管理员!");
        } else if (!user.isAccountNonLocked()) {
            throw new LockedException("该账号已被锁定，请联系管理员!");
        } else if (!user.isAccountNonExpired()) {
            throw new AccountExpiredException("该账号已过期，请联系管理员!");
        } else if (!user.isCredentialsNonExpired()) {
            throw new CredentialsExpiredException("该账户的登录凭证已过期，请重新登录!");
        }
        return user;
    }

}