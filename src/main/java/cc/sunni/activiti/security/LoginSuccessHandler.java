package cc.sunni.activiti.security;

import cc.sunni.activiti.pojo.UserInfoBean;
import cc.sunni.activiti.util.R;
import cn.hutool.json.JSONUtil;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author jl
 * @since 2021/1/27 20:48
 */
@Component
public class LoginSuccessHandler implements AuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        httpServletResponse.setContentType("application/json; charset=utf-8");
        PrintWriter writer = httpServletResponse.getWriter();
        UserInfoBean principal =(UserInfoBean) authentication.getPrincipal();
        writer.print(JSONUtil.toJsonStr(R.ok(principal.getUsername())));
        writer.close();
        httpServletResponse.flushBuffer();
    }
}
