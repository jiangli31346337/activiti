package cc.sunni.activiti.controller;

import cc.sunni.activiti.mapper.ActivitiMapper;
import cc.sunni.activiti.util.R;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jl
 * @since 2021/1/31 11:24
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private ActivitiMapper activitiMapper;

    @GetMapping("/list")
    @ApiOperation("获取用户列表")
    public R list() {
        return R.ok(activitiMapper.selectUser());
    }
}
