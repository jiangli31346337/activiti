package cc.sunni.activiti.mapper;

import cc.sunni.activiti.pojo.UserInfoBean;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author jl
 * @since 2021-01-27 19:54:39
 */
@Mapper
public interface UserDao extends BaseMapper<UserInfoBean> {
	
}
